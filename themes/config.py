# --------------------------------------------------------------#
#                       QTILE-CONFIG
# --------------------------------------------------------------#
import os
import re
import socket
import subprocess
import arcobattery
# from typing import List  # noqa: F401
from libqtile import layout, bar, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen, Rule
from libqtile.command import lazy
from libqtile.layout.floating import Floating
from libqtile.layout.xmonad import MonadTall, MonadWide
from libqtile.layout.bsp import Bsp
from libqtile.layout.max import Max
from libqtile.layout.matrix import Matrix
from libqtile import qtile
from libqtile.widget.groupbox import GroupBox
from colors import gruvbox
from libqtile.widget.volume import Volume
# import keyIndicator
# IMPORT FOR MOUSE CALLBACKS
# import V

#==============================================================#

mod = "mod1"
mod1 = "alt"
mod2 = "control"
home = os.path.expanduser("~")


@lazy.function
def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

@lazy.function
def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)




# ====================  Keybindings  ============================#
keys = [

    Key([mod], "w", lazy.spawn('firefox'), desc="Launch browser"),
    Key([mod], "e", lazy.spawn('xterm nvim'), desc="Launch termite"),
    # Key([mod], "n", lazy.spawn('xterm nvim'), desc="Launch xterm"),
    Key([mod], "d", lazy.spawn('dmenu_run -p "Run: "'), desc="Launch"),
    Key([mod], "t", lazy.spawn('thunar'), desc="Launch thunar"),
    Key([mod], "Return", lazy.spawn('termite'), desc="Launch termite"),

    # Toggle floating and fullscreen
    Key([mod], "f", lazy.window.toggle_fullscreen(), desc="Toggle fullscreen mode"),
    Key([mod, "shift"], "space", lazy.window.toggle_floating(), desc="Toggle fullscreen mode"),

    # Keybindings for resizing 
    Key([mod], "i", lazy.layout.grow()),
    Key([mod], "m", lazy.layout.shrink()),
    # Key([mod], "n", lazy.layout.normalize()),
    Key([mod], "o", lazy.layout.maximize()),
    Key([mod, "control"], "space", lazy.layout.flip()),

    # ------------- change focus -----------#
    Key([mod], "Up", lazy.layout.up()),
    Key([mod], "Down", lazy.layout.down()),
    Key([mod], "Left", lazy.layout.left()),
    Key([mod], "Right", lazy.layout.right()),
    Key([mod], "k", lazy.layout.up()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "l", lazy.layout.right()),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod, "shift"], "c", lazy.window.kill(), desc="Kill focused window"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    #------------------- keybindings finished -----------------------#




# keys = [
#     # Most of our keybindings are in sxhkd file - except these
#     # SUPER + FUNCTION KEYS
#     Key([mod], "f", lazy.window.toggle_fullscreen()),
#     Key([mod], "q", lazy.window.kill()),
#     # SUPER + SHIFT KEYS
#     Key([mod, "shift"], "q", lazy.window.kill()),
#     Key([mod, "shift"], "r", lazy.restart()),
#     # QTILE LAYOUT KEYS
#     Key([mod], "n", lazy.layout.normalize()),
#     Key([mod], "space", lazy.next_layout()),
#     # CHANGE FOCUS
#     Key([mod], "Up", lazy.layout.up()),
#     Key([mod], "Down", lazy.layout.down()),
#     Key([mod], "Left", lazy.layout.left()),
#     Key([mod], "Right", lazy.layout.right()),
#     Key([mod], "k", lazy.layout.up()),
#     Key([mod], "j", lazy.layout.down()),
#     Key([mod], "h", lazy.layout.left()),
#     Key([mod], "l", lazy.layout.right()),
    # RESIZE UP, DOWN, LEFT, RIGHT
    Key(
        [mod, "control"],
        "l",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
    ),
    Key(
        [mod, "control"],
        "Right",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
    ),
    Key(
        [mod, "control"],
        "h",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
    ),
    Key(
        [mod, "control"],
        "Left",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
    ),
    Key(
        [mod, "control"],
        "k",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
    ),
    Key(
        [mod, "control"],
        "Up",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
    ),
    Key(
        [mod, "control"],
        "j",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
    ),
    Key(
        [mod, "control"],
        "Down",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
    ),
#     # FLIP LAYOUT FOR MONADTALL/MONADWIDE
#     Key([mod, "shift"], "f", lazy.layout.flip()),
#     # FLIP LAYOUT FOR BSP
#     Key([mod, "mod1"], "k", lazy.layout.flip_up()),
#     Key([mod, "mod1"], "j", lazy.layout.flip_down()),
#     Key([mod, "mod1"], "l", lazy.layout.flip_right()),
#     Key([mod, "mod1"], "h", lazy.layout.flip_left()),
#     # MOVE WINDOWS UP OR DOWN BSP LAYOUT
#     Key([mod, "shift"], "k", lazy.layout.shuffle_up()),
#     Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
#     Key([mod, "shift"], "h", lazy.layout.shuffle_left()),
#     Key([mod, "shift"], "l", lazy.layout.shuffle_right()),
#     # MOVE WINDOWS UP OR DOWN MONADTALL/MONADWIDE LAYOUT
#     Key([mod, "shift"], "Up", lazy.layout.shuffle_up()),
#     Key([mod, "shift"], "Down", lazy.layout.shuffle_down()),
#     Key([mod, "shift"], "Left", lazy.layout.swap_left()),
#     Key([mod, "shift"], "Right", lazy.layout.swap_right()),
#     # TOGGLE FLOATING LAYOUT
#     Key([mod, "shift"], "space", lazy.window.toggle_floating()),
]

groups = []

# FOR QWERTY KEYBOARDS
group_names = [
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
]


group_labels = [
    "",
    "",
    "",
    "",
    "",
    "",
    "",
]

group_layouts = [
    "monadtall",
    "monadtall",
    "monadtall",
    "monadtall",
    "monadtall",
    "monadtall",
    "monadtall",
]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        )
    )

for i in groups:
    keys.extend(
        [
            # CHANGE WORKSPACES
            Key([mod], i.name, lazy.group[i.name].toscreen()),
            Key([mod], "Tab", lazy.screen.next_group()),
            Key([mod, "shift"], "Tab", lazy.screen.prev_group()),
            Key(["mod1"], "Tab", lazy.screen.next_group()),
            Key(["mod1", "shift"], "Tab", lazy.screen.prev_group()),
            # MOVE WINDOW TO SELECTED WORKSPACE 1-i AND STAY ON WORKSPACE
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
            # MOVE WINDOW TO SELECTED WORKSPACE 1-i AND FOLLOW MOVED WINDOW TO WORKSPACE
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name),
                lazy.group[i.name].toscreen(),
            ),
        ]
    )


def init_layout_theme():
    return {
        "margin": 5,
        "border_width": 2,
        "border_focus": "#ffff44",
        "border_normal": "#2d333f",
    }


layout_theme = init_layout_theme()


layouts = [
    MonadTall( margin=5, border_width=1, border_focus="#00aa44", border_normal="#2d333f"),
    Floating( margin=4, border_width=1, border_focus="#00aa44", border_normal="#ffff44" ),
    MonadWide( margin=6, border_width=1, border_focus="#4c566a", border_normal="#2d333f"),
    Matrix(**layout_theme),
    Bsp(**layout_theme),
    # layout.RatioTile(**layout_theme),
    Max(**layout_theme),
]


# ------------- THEME COLORS -------------

def init_colors():
    return [
        # ["#2e3440", "#2e3440"],  # 0 background
        ["#2e3440", "#2e3440"],  # 0 background
        ["#d8dee9", "#d8dee9"],  # 1 foreground
        ["#2d333f", "#2d333f"],  # 2 background for bar
        ["#bf616a", "#bf616a"],  # 3 red
        ["#a3be8c", "#a3be8c"],  # 4 green
        ["#ebcb8b", "#ebcb8b"],  # 5 yellow
        ["#81a1c1", "#81a1c1"],  # 6 blue
        ["#b48ead", "#b48ead"],  # 7 magenta
        ["#88c0d0", "#88c0d0"],  # 8 cyan
        ["#e5e9f0", "#e5e9f0"],  # 9 white
        ["#4c566a", "#4c566a"],  # 10 grey
        ["#d08770", "#d08770"],  # 11 orange
        ["#8fbcbb", "#8fbcbb"],  # 12 super cyan
        ["#5e81ac", "#5e81ac"],  # 13 super blue
        ["#242831", "#242831"],  # 14 module background
    ]  

colors = init_colors()





# -----------WIDGETS FOR THE BAR------------

def init_widgets_defaults():
    return dict(font="Noto Sans Bold", fontsize=12, padding=2, background=colors[1])

widget_defaults = init_widgets_defaults()

# Qtile bar glyphs

# left = ""
left = ""
right = ""

# MOUSE CALLBACKS


def openCalendar():  # spawn calendar widget
    qtile.cmd_spawn("gsimplecal")


def openHtop():
    qtile.cmd_spawn("alacritty -e htop")


def openMenu():
    qtile.cmd_spawn(
        "rofi -show drun -show-icons "
    )


def powerMenu():
    qtile.cmd_spawn("arcolinux-logout")



# -------------------- widgets --------------------

def init_widgets_list():
    prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())
    widgets_list = [
        widget.Spacer(
            background=colors[2],
            length=10,
        ),
        widget.TextBox(
            font="MesloLGS NF",
            text=left,
            foreground=colors[14],
            background=colors[2],
            padding=0,
            fontsize=18,
        ),
        widget.Image(
            background=colors[14],
            foreground=colors[9],
            filename="~/.config/qtile/icons/arch2.png",
            mouse_callbacks={"Button1": openMenu},
        ),
        widget.TextBox(
            font="MesloLGS NF",
            text=right,
            foreground=colors[14],
            background=colors[2],
            padding=0,
            fontsize=18,
        ),
        widget.Spacer(
            background=colors[2],
            length=5,
        ),







        widget.TextBox(
            font="MesloLGS NF",
            text=left,
            foreground=colors[14],
            background=colors[2],
            padding=0,
            fontsize=18,
        ),
        widget.TextBox(
            font="FontAwesome",
            text="   ",
            foreground=colors[9],
            background=colors[14],
            padding=0,
            mouse_callbacks={"Button1": openHtop},
            fontsize=18,
        ),
        Volume(
            background=colors[14],
            foreground=colors[6],
            format = '{}',
            # mouse_callbacks={"Button1": openMenu},
        ),
        widget.TextBox(
            font="MesloLGS NF",
            text=right,
            foreground=colors[14],
            background=colors[2],
            padding=0,
            fontsize=18,
        ),
        widget.Spacer(
            background=colors[2],
            length=5,
        ),
        widget.TextBox(
            font="MesloLGS NF",
            text=left,
            foreground=colors[14],
            background=colors[2],
            padding=0,
            fontsize=18,
        ),
        widget.CurrentLayoutIcon(
            foreground=colors[9], background=colors[14], scale=0.60
        ),
        widget.TextBox(
            font="MesloLGS NF",
            text=right,
            foreground=colors[14],
            background=colors[2],
            padding=0,
            fontsize=18,
        ),
        widget.Spacer(
            background=colors[2],
            length=5,
        ),

        widget.TextBox(
            font="MesloLGS NF",
            text=left,
            foreground=colors[14],
            background=colors[2],
            padding=0,
            fontsize=18,
        ),
        widget.TextBox(
            font="MesloLGS NF",
            text=" ",
            foreground=colors[6],
            background=colors[14],
            padding=0,
            fontsize=18,
        ),
        widget.WindowName(
            font="Noto Sans Bold",
            fontsize=12,
            foreground=colors[7],
            background=colors[14],
            width=bar.CALCULATED,
            empty_group_string="Desktop",
            margin_y=1,
            max_chars=80,
        ),
        widget.TextBox(
            font="MesloLGS NF",
            text=right,
            foreground=colors[14],
            background=colors[2],
            padding=0,
            fontsize=18,
        ),
        widget.Spacer(
            background=colors[2],
        ),
        widget.TextBox(
            font="MesloLGS NF",
            text=left,
            foreground=colors[14],
            background=colors[2],
            padding=0,
            fontsize=18,
        ),
        widget.GroupBox(
            font="FontAwesome",
            fontsize=16,
            margin_y=3,
            margin_x=0,
            padding_y=0,
            padding_x=9,
            disable_drag=True,
            use_mouse_wheel=True,
            active=colors[9],
            inactive=colors[10],
            rounded=True,
            highlight_color=colors[2],
            block_highlight_text_color=colors[6],
            highlight_method="text",
            this_current_screen_border=colors[4],
            this_screen_border=colors[4],
            other_current_screen_border=colors[14],
            other_screen_border=colors[14],
            foreground=colors[1],
            background=colors[14],
            urgent_border=colors[3],
        ),
        widget.TextBox(
            font="MesloLGS NF",
            text=right,
            foreground=colors[14],
            background=colors[2],
            padding=0,
            fontsize=18,
        ),
        widget.Spacer(
            background=colors[2],
            length=322,
        ),


        #-----------------------------------


        widget.TextBox(
            font="MesloLGS NF",
            text=left,
            foreground=colors[14],
            background=colors[2],
            padding=0,
            fontsize=18,
        ),
        widget.TextBox(
            font="FontAwesome",
            text="  ",
            foreground=colors[13],
            background=colors[14],
            padding=0,
            mouse_callbacks={"Button1": openHtop},
            fontsize=16,
        ),
        widget.TextBox(
            font="Noto Sans Bold Bold",
            foreground=colors[10],
            background=colors[14],
            fontsize=12,
            text="cpu: ",
        ),
        widget.CPU(
            font="Noto Sans Bold",
            measure_mem="G",
            # format="{MemUsed: .1f}G/{MemTotal: .1f}G ",
            format=' {load_percent}%',
            update_interval=5,
            fontsize=12,
            foreground=colors[1],
            background=colors[14],
            mouse_callbacks={"Button1": openHtop},
        ),
        widget.TextBox(
            font="MesloLGS NF",
            text=right,
            foreground=colors[14],
            background=colors[2],
            padding=0,
            fontsize=18,
        ),
        widget.Spacer(
            background=colors[2],
            length=5,
        ),



        # ----------------------------------
        widget.TextBox(
            font="MesloLGS NF",
            text=left,
            foreground=colors[14],
            background=colors[2],
            padding=0,
            fontsize=18,
        ),
        widget.TextBox(
            font="FontAwesome",
            text="  ",
            foreground=colors[13],
            background=colors[14],
            padding=0,
            mouse_callbacks={"Button1": openHtop},
            fontsize=16,
        ),
        widget.TextBox(
            font="Noto Sans Bold Bold",
            foreground=colors[10],
            background=colors[14],
            fontsize=12,
            text="Ram: ",
        ),
        widget.Memory(
            font="Noto Sans Bold",
            measure_mem="G",
            format="{MemUsed: .1f}G",
            # format="{MemUsed: .1f}G/{MemTotal: .1f}G ",
            update_interval=5,
            fontsize=12,
            foreground=colors[1],
            background=colors[14],
            mouse_callbacks={"Button1": openHtop},
        ),
        widget.TextBox(
            font="MesloLGS NF",
            text=right,
            foreground=colors[14],
            background=colors[2],
            padding=0,
            fontsize=18,
        ),
        widget.Spacer(
            background=colors[2],
            length=5,
        ),

        # ----------------------------------
        widget.TextBox(
            font="MesloLGS NF",
            text=left,
            foreground=colors[14],
            background=colors[2],
            padding=0,
            fontsize=18,
        ),
        widget.TextBox(
            font="FontAwesome",
            text="",
            foreground=colors[13],
            background=colors[14],
            mouse_callbacks={"Button1": openCalendar},
            padding=1,
            fontsize=24,
        ),
        widget.Clock(
            font="Noto Sans Bold Bold",
            foreground=colors[1],
            background=colors[14],
            fontsize=12,
            mouse_callbacks={"Button1": openCalendar},
            format=" %a-%d | %H:%M ",
        ),
        widget.TextBox(
            font="MesloLGS NF",
            text=right,
            foreground=colors[14],
            background=colors[2],
            padding=0,
            fontsize=18,
        ),
        widget.Spacer(
            background=colors[2],
            length=5,
        ),

        # ----------------------------------
        widget.TextBox(
            font="MesloLGS NF",
            text=left,
            foreground=colors[14],
            background=colors[2],
            padding=0,
            fontsize=18,
        ),
        arcobattery.BatteryIcon(
            padding=0,
            scale=0.7,
            y_poss=1,
            theme_path=home + "/.config/qtile/icons/battery_icons_horiz",
            update_interval=5,
            background=colors[14],
        ),
        widget.TextBox(
            font="MesloLGS NF",
            text=right,
            foreground=colors[14],
            background=colors[2],
            padding=0,
            fontsize=18,
        ),
        widget.Spacer(
            background=colors[2],
            length=5,
        ),

        # ----------------------------------
        widget.TextBox(
            font="MesloLGS NF",
            text=left,
            foreground=colors[14],
            background=colors[2],
            padding=0,
            fontsize=18,
        ),
        widget.TextBox(
            font="FontAwesome",
            text="",
            foreground=colors[13],
            background=colors[14],
            padding=0,
            fontsize=18,
            mouse_callbacks={"Button1": powerMenu},
        ),
        widget.TextBox(
            font="MesloLGS NF",
            text=right,
            foreground=colors[14],
            background=colors[2],
            padding=0,
            fontsize=18,
        ),
        widget.Spacer(
            background=colors[2],
            length=5,
        ),
        # ----------------------------------
    ]
    return widgets_list


widgets_list = init_widgets_list()


def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1


def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    return widgets_screen2


widgets_screen1 = init_widgets_screen1()
widgets_screen2 = init_widgets_screen2()


def init_screens():
    return [
        Screen(
            top=bar.Bar(
                widgets=init_widgets_screen1(),
                size=23,
                background="#2d333f",
                border_color=["#2d333f", "#2d333f", "#2d333f", "#2d333f"],
                border_width=[0, 0, 0, 0],
                opacity=1,
                margin=[4, 0, 0, 0],
            )
        ),
        Screen(
            top=bar.Bar(
                widgets=init_widgets_screen2(),
                size=26,
                opacity=1,
                margin=[2, 6, 0, 6],
            )
        ),
    ]


screens = init_screens()


# MOUSE CONFIGURATION
mouse = [
    Drag(
        [mod],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        [mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()
    ),
]

dgroups_key_binder = None
dgroups_app_rules = []


main = None


@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser("~")
    subprocess.call([home + "/.config/qtile/scripts/autostart.sh"])


@hook.subscribe.startup
def start_always():
    # Set the cursor to something sane in X
    subprocess.Popen(["xsetroot", "-cursor_name", "left_ptr"])


@hook.subscribe.client_new
def set_floating(window):
    if (
        window.window.get_wm_transient_for()
        or window.window.get_wm_type() in floating_types
    ):
        window.floating = True


floating_types = ["notification", "toolbar", "splash", "dialog"]


follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
        Match(wm_class="Arcolinux-welcome-app.py"),
        Match(wm_class="Arcolinux-tweak-tool.py"),
        Match(wm_class="Arcolinux-calamares-tool.py"),
        Match(wm_class="confirm"),
        Match(wm_class="dialog"),
        Match(wm_class="download"),
        Match(wm_class="error"),
        Match(wm_class="file_progress"),
        Match(wm_class="notification"),
        Match(wm_class="splash"),
        Match(wm_class="toolbar"),
        Match(wm_class="Arandr"),
        Match(wm_class="feh"),
        Match(wm_class="Galculator"),
        Match(wm_class="arcolinux-logout"),
        Match(wm_class="xfce4-terminal"),
        Match(wm_class="Yad"),
        Match(wm_class="pavucontrol"),
        Match(wm_class="Bluetooth"),
    ],
    fullscreen_border_width=0,
    border_width=0,
)
auto_fullscreen = True

focus_on_window_activation = "focus"  # or smart

wmname = "LG3D"
